//
//  GameScene.h
//  SpriteKit-Basics
//

//  Copyright (c) 2016 Thomas Donohue. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene

@end
