//
//  GameOverScene.h
//  SpriteKit-Basics
//
//  Created by Thomas Donohue on 2/17/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameOverScene : SKScene

-(id)initWithSize:(CGSize)size won:(BOOL)won;

@end