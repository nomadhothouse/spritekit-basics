//
//  main.m
//  SpriteKit-Basics
//
//  Created by Thomas Donohue on 2/15/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
